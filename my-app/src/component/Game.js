import React from "react";
import Board from "./Board";

class Game extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            history: [{
                squares: Array(9).fill(null),
            }],
            xIsNext: true,
            stepNumber: 0,
            selected: null
        }
    }

    handleClick(i){
        const history = this.state.history.slice(0, this.state.stepNumber + 1)
        const current = history[history.length - 1];
        const squares = current.squares.slice();
        // Found winner or square already selected
        if(calculateWinner(squares) || squares[i])
            return

        squares[i] = this.state.xIsNext ? 'X' : 'O'
        this.setState({
            history: history.concat([{
                squares: squares,
                col: i%3,
                row: parseInt(i/3),
            }]),
            xIsNext: !this.state.xIsNext,
            stepNumber: history.length,
            selected: null
        })
    }

    jumpTo(step){
        this.setState({
            stepNumber: step,
            xIsNext: (step % 2) === 0,
            selected: step
        })
    }

    render() {
        const history = this.state.history
        const current = history[this.state.stepNumber]
        const winner = calculateWinner(current.squares)

        let status = winner
            ? 'Winner: ' + winner
            : 'Next player: ' + (this.state.xIsNext ? 'X' : 'O');

        const moves = history.map((step, move) => {
            const desc = move
                ? 'Go to move #' + move + ' (' + this.state.history[move].col + ',' + this.state.history[move].row + ')'
                : 'Go to game start'

            let isBold = move === this.state.selected
            const listItem = isBold
                ?
                <li key={move}>
                    <button onClick={() => this.jumpTo(move)}><b>{desc}</b></button>
                </li>
                :
                <li key={move}>
                    <button onClick={() => this.jumpTo(move)}>{desc}</button>
                </li>

            return (
                listItem
            )
        })

        return (
            <div className="game">
                <div className="game-board">
                    <Board
                        squares={current.squares}
                        onClick={(i) => this.handleClick(i)}
                    />
                </div>
                <div className="game-info">
                    <div>{status}</div>
                    <ol>{moves}</ol>
                </div>
            </div>
        );
    }
}

export default Game;

function calculateWinner(squares) {
    const lines = [
        [0, 1, 2],
        [3, 4, 5],
        [6, 7, 8],
        [0, 3, 6],
        [1, 4, 7],
        [2, 5, 8],
        [0, 4, 8],
        [2, 4, 6],
    ];
    for (let i = 0; i < lines.length; i++) {
        const [a, b, c] = lines[i];
        if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
            return squares[a];
        }
    }
    return null;
}
